﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using StepChartParser.Application;
using StepChartParser.Application.Abstractions;
using StepChartParser.Core;
using StepChartParser.Infrastructure;
using StepChartParser.Infrastructure.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StepChartParser.DependencyInjection
{
  public static class ServiceCollectionExtensions
  {
    public static IServiceCollection AddApplication(this IServiceCollection builder)
      => builder.AddMediatR(typeof(ProcessSongsHandler).Assembly);

    public static IServiceCollection AddInfrastructure(this IServiceCollection builder)
    {
      var concreteAssembly = typeof(SongRepository).Assembly;
      var interfaceAssembly = typeof(ISongRepository).Assembly;
      var concreteTypes = concreteAssembly.GetTypes().Where(t => !t.IsAbstract && !t.IsInterface);
      foreach(var type in concreteTypes)
      {
        var interfaces = type.GetInterfaces().Where(i => i.Assembly == interfaceAssembly).ToArray();
        if (!interfaces.Any())
        {
          continue;
        }
        foreach(var i in interfaces)
        {
          builder.AddTransient(i, type);
        }
      }
      return builder;
    }

    public static ConfigurableSongProcessor WhenSong(this IServiceCollection builder, Func<Song,bool> shouldAffectSong)
    {
      var processor = new ConfigurableSongProcessor(shouldAffectSong);
      builder.AddSingleton<ISongProcessor>(processor);
      return processor;
    }

    public static ConfigurableChartProcessor WhenChart(this IServiceCollection builder, Func<Chart, bool> shouldAffectChart)
    {
      var processor = new ConfigurableChartProcessor(shouldAffectChart);
      builder.AddSingleton<IChartProcessor>(processor);
      return processor;
    }

  }
}
