﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StepChartParser.Core
{
  public sealed class Field
  {
    public Field(string value, int order)
    {
      Order = order;
      Value = value;
    }
    public string Value { get; set; }
    public int Order { get; set; }
    public override string ToString()
    {
      return Value;
    }
  }
}
