﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace StepChartParser.Core
{
  public sealed class Song
  {
    private static Regex BgaRegex = new Regex(@"[^=]{2,}\.(mpg|mkv|mp4|avi)", RegexOptions.IgnoreCase | RegexOptions.Compiled);
    public Song(string filePath, IEnumerable<string> otherFiles)
    {
      Fields = new Dictionary<string, Field>(StringComparer.OrdinalIgnoreCase);
      Charts = new List<Chart>();
      FilePath = filePath;
      OtherFiles = otherFiles.Distinct(StringComparer.OrdinalIgnoreCase).ToArray();
    }
    public IDictionary<string,Field> Fields { get; }
    public ICollection<Chart> Charts { get; private set; }
    private IEnumerable<string> BgChanges => BgaRegex.Matches(GetField("bgchanges")).Select(match => match.Value);
    public IEnumerable<string> BgaFileNames => BgChanges.Where(fileName => !fileName.StartsWith("black.", StringComparison.OrdinalIgnoreCase));
    public string FilePath { get; }
    public string[] OtherFiles { get; }
    public bool HasFile(string file)
      => OtherFiles.Contains(file, StringComparer.OrdinalIgnoreCase);
    
    public string MusicFile => GetField("Music");
    public void ReplaceBga(string oldBga, string newBga)
    {
      SetField("bgchanges", GetField("bgchanges").Replace(oldBga, newBga), 11);
    }
    public string BackgroundImageFile
    {
      get => GetField("background");
      set => SetField("background", value, 9);
    }
    public string BannerFile
    {
      get => GetField("banner");
      set => SetField("banner", value, 10);
    }
    public string PreviewVideoFile => GetField("previewvid");
    
    public void SetField(string name, string value, int defaultOrder)
    {
      if (Fields.ContainsKey(name))
      {
        Fields[name].Value = value;
        return;
      }
      AddField(name, new Field(value, defaultOrder));
    }
    private void AddField(string key, Field field)
    {
      if (Fields.ContainsKey(key))
      {
        throw new Exception(key + " already exists");
      }
      foreach (var keyValue in Fields)
      {
        if (keyValue.Value.Order >= field.Order)
        {
          keyValue.Value.Order = keyValue.Value.Order + 1;
        }
      }
      Fields[key] = field;
    }
    private string GetField(string name) => Fields.TryGetValue(name, out var value) ? value.Value : string.Empty;
    public string Name => GetField("title");
    public bool HasChart(Chart chart) => Charts.Any(c => c.Type==chart.Type && c.Difficulty==chart.Difficulty && c.IsFromPro == chart.IsFromPro && c.IsFromInfinity ==chart.IsFromInfinity && c.IsCustomStep==chart.IsCustomStep);
    public void AddChart(Chart chart)
    {
      Charts.Add(chart);
    }
    public SongType Type {
      get => Enum.TryParse<SongType>(GetField("songtype"),true, out var result) ? result : SongType.Unknown;
      set => SetField("SONGTYPE", value.ToString().ToUpper(), 5);
    }
    public bool HasDuplicates()
      => GroupChartsByNotes().Any(grouping => grouping.Skip(1).Any());
    public void RemoveDuplicateCharts()
    {
      Charts = GroupChartsByNotes().Select(grouping => grouping.First()).ToList();
    }
    private IEnumerable<IGrouping<string, Chart>> GroupChartsByNotes()
      => Charts.GroupBy(chart => $"{chart.TrimmedNotes}", StringComparer.OrdinalIgnoreCase);
  }
}
