﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StepChartParser.Core
{
  public enum ChartType
  {
    Single,
    Double,
    CoOp,
    Unknown
  }
}
