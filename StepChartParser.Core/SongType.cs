﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StepChartParser.Core
{
  public enum SongType
  {
    Arcade,
    Shortcut,
    FullSong,
    Remix,
    Unknown
  }
}
