﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace StepChartParser.Core
{
  public sealed class Chart
  {
    private static Regex _whitespace = new Regex(@"\s+", RegexOptions.Compiled);
    public Chart(Song song)
    {
      Song = song;
      Fields = new Dictionary<string, Field>(StringComparer.OrdinalIgnoreCase);
    }
    public Song Song { get; }
    public void AddField(string key, Field field)
    {
      if (Fields.ContainsKey(key))
      {
        throw new Exception(key + " already exists");
      }
      foreach (var keyValue in Fields)
      {
        if (keyValue.Value.Order >= field.Order)
        {
          keyValue.Value.Order = keyValue.Value.Order + 1;
        }
      }
      Fields[key] = field;
    }
    private string StepsType
    {
      get => GetField("stepstype");
      set => SetField("stepstype", value, 1);
    }
    public ChartType Type
    {
      get => StepsType.Equals("pump-single", StringComparison.OrdinalIgnoreCase) ? ChartType.Single
: StepsType.Equals("pump-double", StringComparison.OrdinalIgnoreCase) ? ChartType.Double
: StepsType.Equals("pump-routine", StringComparison.OrdinalIgnoreCase) ? ChartType.CoOp
: ChartType.Unknown;
      set
      {
        StepsType = value == ChartType.CoOp ? "pump-routine"
          : value == ChartType.Single ? "pump-single"
          : value == ChartType.Double ? "pump-double"
          : "pump-unknown";
      }
    }
    public void SetField(string name, string value, int defaultOrder)
    {
      if (Fields.ContainsKey(name))
      {
        Fields[name].Value = value;
        return;
      }
      AddField(name, new Field(value, defaultOrder));
    }
    private string GetField(string name) => Fields.TryGetValue(name, out var value) ? value.Value : string.Empty;
    public int? Difficulty
    {
      get => int.TryParse(GetField("meter"), out var value) ? (int?)value : null;
      set => SetField("METER", value.ToString(), 4);
    }
    public IEnumerable<string> Tags {
      get => _whitespace.Split(GetField("description")).Where(tag=>!string.IsNullOrWhiteSpace(tag));
      private set => SetField("description", string.Join(" ", value.Where(tag=>!string.IsNullOrWhiteSpace(tag))), 3);
    }
    public bool HasTag(string tag) => Tags.Contains(tag.Trim(), StringComparer.OrdinalIgnoreCase);
    public void RemoveTag(string tag) => Tags = Tags.Except(new[] { tag }, StringComparer.OrdinalIgnoreCase);
    public void AddTag(string tag) => Tags = Tags.Union(new[] { tag }, StringComparer.OrdinalIgnoreCase);
    public bool IsFromInfinity
    {
      get => HasTag("infinity");
      set
      {
        if (value && !IsFromInfinity)
        {
          AddTag("infinity");
        }
        else if (!value && IsFromInfinity)
        {
          RemoveTag("infinity");
        }

      }
    }
    public bool IsFromPumpJump => HasTag("jump");
    public bool IsFromPro {
      get => HasTag("pro");
      set
      {
        if(value && !IsFromPro)
        {
          AddTag("pro");
        } else if(!value && IsFromPro)
        {
          RemoveTag("pro");
        }
      }
    }
    public bool IsCustomStep => GetField("description").Contains("ucs", StringComparison.OrdinalIgnoreCase);
    public bool IsQuest => HasTag("quest");
    public string ChartName
    {
      get => GetField("chartname");
      set
      {
        SetField("chartname", value, 4);
      }
    }
    public bool IsPerformance => Tags.Any(t => t.Contains("dp", StringComparison.OrdinalIgnoreCase) || t.Contains("sp", StringComparison.OrdinalIgnoreCase));
    public bool IsInfoBarEnabled
    {
      get => HasTag("Infobar");
      set {
        if (value && !IsInfoBarEnabled)
        {
          AddTag("Infobar");
        }
        else if (!value && IsInfoBarEnabled) {
          RemoveTag("Infobar");
        }
      }
    }
    public IDictionary<string, Field> Fields { get; }

    public string Notes => GetField("notes");
    public string TrimmedNotes => RealNotes(this);
    private static Regex _doublesNotesFinder = new Regex(@"[0-9]{10,10}", RegexOptions.Compiled);
    private static Regex _singlesNotesfinder = new Regex(@"[0-9]{5,5}", RegexOptions.Compiled);
    private string RealNotes(Chart chart) => string.Join(@"
", (chart.Type == ChartType.Single ? _singlesNotesfinder : _doublesNotesFinder).Matches(chart.Notes).Select(match => match.Value));
  }
}
