﻿using Microsoft.Extensions.Logging;
using StepChartParser.Core;
using StepChartParser.Infrastructure.Abstractions;
using StepChartParser.Infrastructure.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace StepChartParser.Infrastructure
{
  public sealed class SongRepository : ISongRepository
  {
    private readonly ILogger<SongRepository> _logger;
    private static Regex _songSplitRegex = new Regex(@"\/\/\-{4,}.*?\n", RegexOptions.IgnoreCase | RegexOptions.Compiled);
    private static Regex _fieldSplitRegex = new Regex(@"(?<!\\):", RegexOptions.IgnoreCase | RegexOptions.Compiled);
    private static Regex _propertySplitRegex = new Regex(@"(?<!\\);", RegexOptions.IgnoreCase | RegexOptions.Compiled);
    public SongRepository(ILogger<SongRepository> logger)
    {
      _logger = logger;
    }

    public Task DeleteSong(Song song)
    {
      _logger.LogInformation("Delete " + song.FilePath);
      return Task.CompletedTask;
    }

    public async IAsyncEnumerable<Song> LoadSongs(string directory)
    {
      var filePaths = Directory.GetFiles(directory, "*.ssc", SearchOption.AllDirectories);

      foreach(var filePath in filePaths)
      {
        Song song;
        try
        {
           song = await GetSongFromFile(filePath);
        }
        catch (Exception e)
        {
          _logger.LogError(e, $"There was an error while parsing {filePath}");
          continue;
        }
        yield return song;
      }
    }
    private async Task<Song> GetSongFromFile(string filePath)
    {
      var directory = new FileInfo(filePath).Directory.FullName;
      var otherFiles = Directory.GetFiles(directory,"*.*",SearchOption.TopDirectoryOnly).Select(Path.GetFileName);
      var stream = new StreamReader(filePath);
      var fileSplit = _songSplitRegex.Split(await stream.ReadToEndAsync());
      var song = new Song(filePath, otherFiles);
      PopulateFields(song.Fields, fileSplit[0]);
      for(var i=1; i < fileSplit.Length; i++)
      {
        var chart = new Chart(song);
        PopulateFields(chart.Fields, fileSplit[i]);
        song.Charts.Add(chart);
      }
      _logger.LogInformation($"Parsed {filePath}");
      stream.Close();
      return song;
    }
    private void PopulateFields(IDictionary<string,Field> fields, string propertyString)
    {
      var entries = _propertySplitRegex.Split(propertyString).Select(property => property).ToArray();
      for (var i= 0;i <entries.Length;i++)
      {
        var entry = entries[i];
        if (string.IsNullOrWhiteSpace(entry))
        {
          continue;
        }
        var keyValue = _fieldSplitRegex.Split(entry);
        var key = keyValue[0].Trim().Substring(1);
        var value = keyValue.Length > 1 ? string.Join(":",keyValue.Skip(1)):string.Empty;
        fields[key] = new Field(value, i); 
      }
    }

    public async Task SaveSong(Song song)
    {
      var writer = new StreamWriter(song.FilePath, false);
      var newContent = SongString(song);
      await writer.WriteAsync(newContent);
      writer.Close();
    }
    private static string SongString(Song song)
    {
      var builder = new StringBuilder();
      foreach(var field in song.Fields.OrderBy(field => field.Value.Order))
      {
        builder.AppendLine($"#{field.Key.ToUpper()}:{field.Value};");
      }
      builder.AppendLine();
      builder.AppendLine();
      foreach(var chart in song.Charts.OrderBy(chart=>chart.Fields["meter"].Value,new SemiNumericComparer()))
      {
        builder.AppendLine(ChartString(chart));
      }
      return builder.ToString();
    }
    private static string ChartString(Chart chart)
    {
      var builder = new StringBuilder();
      builder.AppendLine($"//---------------{chart.Fields.GetDefaultToEmpty("stepstype")} {chart.Fields["meter"]} - {chart.Fields.GetDefaultToEmpty("description")}----------------");
      foreach (var field in chart.Fields.OrderBy(field=>field.Value.Order))
      {
        builder.AppendLine($"#{field.Key.ToUpper()}:{field.Value};");
      }
      builder.AppendLine();
      builder.AppendLine();
      return builder.ToString();
    }
  }
}
