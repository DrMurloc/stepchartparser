﻿using StepChartParser.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace StepChartParser.Infrastructure.Helpers
{
  public static class DictionaryExtensions
  {
    public static string GetDefaultToEmpty(this IDictionary<string, Field> dictionary, string key) => dictionary.TryGetValue(key, out var value) ? value.Value : "";
  }
}
