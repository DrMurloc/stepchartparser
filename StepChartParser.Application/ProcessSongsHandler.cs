﻿using MediatR;
using StepChartParser.Application.Abstractions;
using StepChartParser.Infrastructure.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace StepChartParser.Application
{
  public sealed class ProcessSongsHandler : IRequestHandler<ProcessSongsCommand>
  {
    private readonly ISongRepository _songRepository;
    private readonly ISongProcessor[] _songProcessors;
    private readonly IChartProcessor[] _chartProcessors;
    public ProcessSongsHandler(ISongRepository songRepository,
      IEnumerable<ISongProcessor> songProcessors, 
      IEnumerable<IChartProcessor> chartProcessors)
    {
      _songRepository = songRepository;
      _songProcessors = songProcessors.ToArray();
      _chartProcessors = chartProcessors.ToArray();
    }
    public async Task<Unit> Handle(ProcessSongsCommand request, CancellationToken cancellationToken)
    {
      await foreach (var song in _songRepository.LoadSongs(request.FilePath).WithCancellation(cancellationToken))
      {
        var needsSave = false;
        foreach (var songProcessor in _songProcessors)
        {
          if (songProcessor.ShouldDelete(song))
          {
            await _songRepository.DeleteSong(song);
            continue;
          }
          if (songProcessor.ShouldModify(song))
          {
            songProcessor.Modify(song);
            needsSave = true;
          }
        }
        var charts = song.Charts.ToArray();
        foreach(var chart in charts)
        {
          foreach(var chartProcessor in _chartProcessors)
          {
            if (chartProcessor.ShouldDelete(chart))
            {
              song.Charts.Remove(chart);
              needsSave = true;
              continue;
            }
            if (chartProcessor.ShouldModify(chart))
            {
              chartProcessor.Modify(chart);
              needsSave = true;
            }
          }
        }
        if (needsSave)
        {
          await _songRepository.SaveSong(song);
        }
      }
      return Unit.Value;
    }
  }
}
