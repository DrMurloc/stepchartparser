﻿using StepChartParser.Application.Abstractions;
using StepChartParser.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace StepChartParser.Application
{
  public sealed class ConfigurableSongProcessor : ISongProcessor
  {

    private readonly Func<Song, bool> _shouldAffectSong;
    public ConfigurableSongProcessor(Func<Song,bool> shouldAffectSong)
    {
      _shouldAffectSong = shouldAffectSong;
    }

    public ConfigurableSongProcessor DeleteIt()
    {
      _shouldDelete = _shouldAffectSong;
      return this;
    }
    public ConfigurableSongProcessor ModifyIt(Action<Song> modify)
    {
      _shouldModify = _shouldAffectSong;
      _modify = modify;
      return this;
    }

    private Func<Song, bool> _shouldDelete = _ => false;
    public bool ShouldDelete(Song song) => _shouldDelete(song);

    private Func<Song, bool> _shouldModify = _ => false;
    public bool ShouldModify(Song song) => _shouldModify(song);

    private Action<Song> _modify = _ => { };
    public void Modify(Song song) => _modify(song);

  }
}
