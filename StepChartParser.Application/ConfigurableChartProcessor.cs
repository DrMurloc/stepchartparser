﻿using StepChartParser.Application.Abstractions;
using StepChartParser.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace StepChartParser.Application
{
  public sealed class ConfigurableChartProcessor : IChartProcessor
  {

    private readonly Func<Chart, bool> _shouldAffectChart;
    public ConfigurableChartProcessor(Func<Chart,bool> shouldAffectChart)
    {
      _shouldAffectChart = shouldAffectChart;
    }

    public ConfigurableChartProcessor DeleteIt()
    {
      _shouldDelete = _shouldAffectChart;
      return this;
    }
    public ConfigurableChartProcessor ModifyIt(Action<Chart> modify)
    {
      _shouldModify = _shouldAffectChart;
      _modify = modify;
      return this;
    }

    private Func<Chart, bool> _shouldDelete = _ => false;
    public bool ShouldDelete(Chart Chart) => _shouldDelete(Chart);

    private Func<Chart, bool> _shouldModify = _ => false;
    public bool ShouldModify(Chart Chart) => _shouldModify(Chart);

    private Action<Chart> _modify = _ => { };
    public void Modify(Chart Chart) => _modify(Chart);

  }
}
