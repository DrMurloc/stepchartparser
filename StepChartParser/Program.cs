﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using StepChartParser.Application.Abstractions;
using StepChartParser.ConsoleApp;
using StepChartParser.Core;
using StepChartParser.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace StepChartParser
{
  public sealed class Program
  {
    private const string StepP1Directory = @"C:\Repos\StepP1\Songs";
    public static async Task Main(string[] args)
    {
      var application = BuildApplication();

      //application.ReorganizeAllFiles();
      //application.ConvertHalfDoubleChartsToDouble();
      application.RemoveUcsCharts();
      //application.RemoveQuestCharts();
      //application.FillMissingAndEmptyDescriptions();
      application.RemoveNewTags();
      //application.RemoveHiddenTags();
      //application.RemoveTrainTags();
      //application.RemoveAnotherTags();
      //application.AddInfinityTags();
      //application.AddProTags();
      application.AddHalfDoubleTags();
      //application.ConvertPumpCouplesToRoutine();
      //application.RemoveLightsCabinetCharts();
      //application.RemoveChartsWithNoType();
      //application.RemoveDuplicateCharts();
      //application.FixSongTypes();
      //application.ApplyDifficultyChanges("Arcade||The Quick Brown Fox Jumps Over The Lazy Dog||Double||14||15");
      //application.AutoSetBackgroundImages();
      //application.AutoSetBgas();

      await application.BuildServiceProvider()
        .GetRequiredService<IMediator>()
        .Send(new ProcessSongsCommand(StepP1Directory));
      
    }
    private static IServiceCollection BuildApplication()
    {
      return new ServiceCollection()
        .AddApplication()
        .AddLogging(builder=>
        {
          builder.AddConsole();
        })
        .AddInfrastructure();

    }
  }
}
