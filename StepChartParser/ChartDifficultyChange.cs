﻿using StepChartParser.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StepChartParser.ConsoleApp
{
  public sealed class ChartDifficultyChange
  {
    public SongType SongType { get; set; }
    public string SongName { get; set; }
    public ChartType ChartType { get; set; }
    public int OldDifficulty { get; set; }
    public int NewDifficulty { get; set; }
    public bool WasApplied { get; set; } = false;
    public List<Chart> AppliedCharts = new List<Chart>();

    /*
     * Example format:
     * 
Arcade||CROSS OVER feat. LyuU||Double||14||15
Arcade||The Quick Brown Fox Jumps Over The Lazy Dog||Double||14||15
Arcade||The Quick Brown Fox Jumps Over The Lazy Dog||Single||17||16
Arcade||The Quick Brown Fox Jumps Over The Lazy Dog||Single||20||19
Arcade||Utsushiyo No Kaze feat. Kana||Single||16||17
Arcade||Utsushiyo No Kaze feat. Kana||Double||19||20
Arcade||Achluoias||Single||8||9
Arcade||Achluoias||Double||12||13
Arcade||Achluoias||Single||18||17
     */
    public static IEnumerable<ChartDifficultyChange> ParseChanges(string changeInputs)
      => changeInputs.Split(@"
").Where(s => !string.IsNullOrWhiteSpace(s))
      .Select(change =>
      {
        var split = change.Split("||");
        return new ChartDifficultyChange
        {
          SongType = Enum.Parse<SongType>(split[0], true),
          SongName = split[1].Trim(),
          ChartType = Enum.Parse<ChartType>(split[2], true),
          OldDifficulty = int.Parse(split[3]),
          NewDifficulty = int.Parse(split[4])
        };
      });
  }
}
