﻿using Microsoft.Extensions.DependencyInjection;
using StepChartParser.Core;
using System;
using StepChartParser.DependencyInjection;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using StepChartParser.ConsoleApp;

namespace StepChartParser
{
  public static class SongRefactors
  {
    public static IServiceCollection FillMissingAndEmptyDescriptions(this IServiceCollection builder)
    {
      builder.WhenChart(chart => string.IsNullOrWhiteSpace(chart.Fields.GetValue("description")))
        .ModifyIt(chart => {
          var letter = chart.Fields.GetValue("stepstype").Contains("single") ? "s" :
          chart.Fields.GetValue("stepstype").Contains("double") ? "d" : "";
          var newDescription = letter + chart.Fields.GetValue("meter");
          if (chart.Fields.ContainsKey("description"))
          {
            chart.Fields["description"].Value = newDescription;
          }
          else
          {
            chart.AddField("description", new Field(newDescription, 4));
          }
        });
      return builder;
    }
    public static IServiceCollection FixSongTypes(this IServiceCollection builder)
    {
      builder.WhenSong(song => song.Type != SongType.FullSong && song.FilePath.Contains("Full Song",StringComparison.OrdinalIgnoreCase))
        .ModifyIt(song => song.Type = SongType.FullSong);
      builder.WhenSong(song => song.Type != SongType.Shortcut && song.FilePath.Contains("Shortcut", StringComparison.OrdinalIgnoreCase))
        .ModifyIt(song => song.Type = SongType.Shortcut);
      builder.WhenSong(song => song.Type != SongType.Remix && song.FilePath.Contains("Remix", StringComparison.OrdinalIgnoreCase))
        .ModifyIt(song => song.Type = SongType.Remix);
      return builder;
    }
    public static IServiceCollection ConvertDifficulty99ToCoOp(this IServiceCollection builder)
    {
      builder.WhenChart(chart => chart.Difficulty == 99 && chart.Type == ChartType.Double)
        .ModifyIt(chart => chart.Type = ChartType.CoOp);
      return builder;
    }
    public static IServiceCollection PopulateCoOpInfobars(this IServiceCollection builder)
    {
      builder.WhenChart(chart => chart.Type == ChartType.CoOp && !chart.IsInfoBarEnabled)
        .ModifyIt(chart =>
        {
          chart.IsInfoBarEnabled = true;
          chart.ChartName = "2 Player CoOp";
        });
      return builder;
    }
    public static IServiceCollection ConvertPumpCouplesToRoutine(this IServiceCollection builder)
    {
      builder.WhenChart(chart => chart.Fields.GetValue("stepstype").Equals("pump-couple", StringComparison.OrdinalIgnoreCase))
        .ModifyIt(chart =>
        {
          chart.Fields["stepstype"].Value = "pump-routine";
        });
      return builder;
    }
    public static IServiceCollection RemoveLightsCabinetCharts(this IServiceCollection builder)
    {
      builder.WhenChart(chart => chart.Fields.GetValue("stepstype").Equals("lights-cabinet", StringComparison.OrdinalIgnoreCase))
        .DeleteIt();
      return builder;
    }
    public static IServiceCollection ConvertHalfDoubleChartsToDouble(this IServiceCollection builder)
    {
      builder.WhenChart(chart => chart.Fields.GetValue("stepstype").Equals("pump-halfdouble", StringComparison.OrdinalIgnoreCase))
        .ModifyIt(chart =>
        {
          chart.Fields["stepstype"].Value = "pump-double";
        });
      return builder;
    }
    public static IServiceCollection RemoveChartsWithNoType(this IServiceCollection builder)
    {
      builder.WhenChart(chart => string.IsNullOrWhiteSpace(chart.Fields.GetValue("stepstype")))
        .DeleteIt();
      return builder;
    }
    public static IServiceCollection RemoveUcsCharts(this IServiceCollection builder)
    {
      builder.WhenChart(chart => chart.IsCustomStep).DeleteIt();
      return builder;
    }
    public static IServiceCollection RemoveQuestCharts(this IServiceCollection builder)
    {
      builder.WhenChart(chart => chart.HasTag("quest")).DeleteIt();
      return builder;
    }
    public static IServiceCollection RemoveTrainTags(this IServiceCollection builder)
    {

      builder.WhenChart(chart => chart.HasTag("train"))
        .ModifyIt(chart => chart.RemoveTag("train"));
      return builder;
    }
    public static IServiceCollection RemoveHiddenTags(this IServiceCollection builder)
    {
      builder.WhenChart(chart => chart.HasTag("hidden"))
        .ModifyIt(chart=> chart.RemoveTag("hidden"));
      return builder;
    }
    public static IServiceCollection AddInfinityTags(this IServiceCollection builder)
    {
      builder.WhenChart(chart => chart.Song.FilePath.Contains("07-INFINITY") && !chart.HasTag("infinity"))
        .ModifyIt(chart => chart.AddTag("Infinity"));
      builder.WhenChart(chart => chart.HasTag("(inf)"))
        .ModifyIt(chart => {
          chart.RemoveTag("(inf)");
          if(!chart.HasTag("Infinity"))
          {
            chart.AddTag("Infinity");
          }
        });
      return builder;
    }
    public static IServiceCollection RemoveDuplicateCharts(this IServiceCollection builder)
    {
      builder.WhenSong(song =>song.HasDuplicates())
        .ModifyIt(song=> song.RemoveDuplicateCharts());
      return builder;
    }
    public static IServiceCollection AddProTags(this IServiceCollection builder)
    {
      builder.WhenChart(chart => chart.Song.FilePath.Contains("06-PRO~PRO2") && !chart.HasTag("pro"))
        .ModifyIt(chart => chart.AddTag("pro"));
      builder.WhenChart(chart => chart.Song.FilePath.Contains("06-PRO~PRO2") && chart.HasTag("infinity") && chart.HasTag("pro"))
        .ModifyIt(chart => chart.RemoveTag("infinity"));
      return builder;
    }
    public static IServiceCollection RemoveNewTags(this IServiceCollection builder)
    {
      builder.WhenChart(chart => chart.HasTag("new"))
        .ModifyIt(chart => chart.RemoveTag("new"));
      return builder;
    }
    public static IServiceCollection RemoveAnotherTags(this IServiceCollection builder)
    {
      builder.WhenChart(chart => chart.HasTag("another"))
        .ModifyIt(chart => chart.RemoveTag("another"));
      return builder;
    }
    private static Regex _doublesNotesFinder = new Regex(@"[0-9]{10,10}", RegexOptions.Compiled);
    private static Regex _halfDoublesCheck = new Regex(@"^00[0-9]{6,6}00$", RegexOptions.Compiled);
    public static IServiceCollection AddHalfDoubleTags(this IServiceCollection builder)
    {
      builder.WhenChart(chart => chart.Fields.GetValue("stepstype").Contains("double")
        && chart.HasTag("HalfDouble")
        && _doublesNotesFinder.Matches(chart.Fields.GetValue("notes")).Any(m => !_halfDoublesCheck.IsMatch(m.Value)))
        .ModifyIt(chart => chart.RemoveTag("HalfDouble"));

      builder.WhenChart(chart => chart.Fields.GetValue("stepstype").Contains("double")
        && !chart.HasTag("HalfDouble")
        && _doublesNotesFinder.Matches(chart.Fields.GetValue("notes")).All(m => _halfDoublesCheck.IsMatch(m.Value)))
        .ModifyIt(chart => chart.AddTag("HalfDouble"));
      return builder;
    }


    private static Regex _previewRegex = new Regex(@"_P\.(mpg|mp4|avi|mkv)$", RegexOptions.IgnoreCase | RegexOptions.Compiled);
    private static Regex _bgaRegex = new Regex(@"\.(mpg|mp4|avi|mkv)$", RegexOptions.IgnoreCase | RegexOptions.Compiled);
    public static IServiceCollection AutoSetBgas(this IServiceCollection builder)
    {
      builder.WhenSong(song => song.BgaFileNames.Any(fileName => !song.HasFile(fileName)) && song.OtherFiles.Any(fileName => _bgaRegex.IsMatch(fileName) && !_previewRegex.IsMatch(fileName)))
        .ModifyIt(song =>
        {
          var newBga = song.OtherFiles.FirstOrDefault(fileName => _bgaRegex.IsMatch(fileName) && !_previewRegex.IsMatch(fileName));
          foreach (var oldBga in song.BgaFileNames.Where(fileName => !song.HasFile(fileName)))
          {
            song.ReplaceBga(oldBga, newBga);
          }
        });
      return builder;
       
    }
    private static bool IsImage(string file)
      => file.EndsWith(".jpg", StringComparison.OrdinalIgnoreCase)
      || file.EndsWith(".png", StringComparison.OrdinalIgnoreCase);
    private static bool IsValidBackgroundImage(string file)
      => IsImage(file) && !file.Contains("_") && file.Contains("-wide", StringComparison.OrdinalIgnoreCase);
    private static bool IsValidBanner(string file)
      => IsImage(file) && !(file.Contains("_") || file.Contains("-wide", StringComparison.OrdinalIgnoreCase));
    public static IServiceCollection AutoSetBackgroundImages(this IServiceCollection builder)
    {
      builder.WhenSong(song => !song.HasFile(song.BackgroundImageFile) && song.OtherFiles.Any(file => IsValidBanner(file) || IsValidBackgroundImage(file)))
        .ModifyIt(song => song.BackgroundImageFile = song.OtherFiles.FirstOrDefault(IsValidBackgroundImage) ?? song.OtherFiles.First(IsValidBanner));
      return builder;
    }
    public static IServiceCollection AutoSetBannerImages(this IServiceCollection builder)
    {
      builder.WhenSong(song => !song.HasFile(song.BannerFile) && song.OtherFiles.Any(IsValidBanner))
        .ModifyIt(song => song.BannerFile = song.OtherFiles.OrderBy(fileName => fileName.Length).First(IsValidBanner));
      return builder;
    }
    public static IServiceCollection ReorganizeAllFiles(this IServiceCollection builder)
    {
      builder.WhenSong(song => true)
        .ModifyIt(_ => { });
      return builder;
    }
    public static void ApplyDifficultyChanges(this IServiceCollection builder, string changeListString)
    {

      var changes = ChartDifficultyChange.ParseChanges(changeListString);
      var alreadyUpdated = new HashSet<Chart>();
      foreach (var change in changes)
      {
        builder.WhenChart(c => !c.IsFromInfinity && !c.IsFromPro && !alreadyUpdated.Contains(c) && c.Song.Type == change.SongType && c.Song.Name.Equals(change.SongName, StringComparison.OrdinalIgnoreCase) && c.Type == change.ChartType && c.Difficulty == change.OldDifficulty)
         .ModifyIt(chart =>
         {
           if (change.WasApplied)
           {

           }
           chart.Difficulty = change.NewDifficulty;
           change.WasApplied = true;
           change.AppliedCharts.Add(chart);
           alreadyUpdated.Add(chart);
         });
      }
    }
    public static string GetValue(this IDictionary<string, Field> dictionary, string fieldName)
      => dictionary.TryGetValue(fieldName, out var value) ? value.Value : "";
  }
}
