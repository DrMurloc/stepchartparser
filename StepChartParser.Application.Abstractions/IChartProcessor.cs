﻿using StepChartParser.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace StepChartParser.Application.Abstractions
{
  public interface IChartProcessor
  {
    bool ShouldModify(Chart chart);
    void Modify(Chart chart);
    bool ShouldDelete(Chart chart);
  }
}
