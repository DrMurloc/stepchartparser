﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace StepChartParser.Application.Abstractions
{
  public sealed class ProcessSongsCommand : IRequest
  {
    public ProcessSongsCommand(string filePath)
    {
      FilePath = filePath;
    }
    public string FilePath { get; }
  }
}
