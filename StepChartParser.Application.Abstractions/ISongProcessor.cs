﻿using StepChartParser.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace StepChartParser.Application.Abstractions
{
  public interface ISongProcessor
  {
    bool ShouldModify(Song song);
    void Modify(Song song);
    bool ShouldDelete(Song song);
  }
}
