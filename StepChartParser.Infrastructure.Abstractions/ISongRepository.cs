﻿using StepChartParser.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace StepChartParser.Infrastructure.Abstractions
{
  public interface ISongRepository
  {
    IAsyncEnumerable<Song> LoadSongs(string directory);
    Task SaveSong(Song song);
    Task DeleteSong(Song song);
  }
}
